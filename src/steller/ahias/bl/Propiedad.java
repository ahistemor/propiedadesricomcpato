package steller.ahias.bl;

/**
 * @autor Ahías Steller Mora
 * @version v1.0
 * @since 10/02/2021
 *
 * Esta clase gestiona cada una de las propiedades del sistema
 **/

public class Propiedad {


    //atributos
    private int codigo;
    private String nombre;
    private int cuartos;
    private String provincia;
    private double alquiler;

    //constructores

    /**
    * Este es el método contructor de la clase
    * Es el constructor por defecto, no recibe parámetros
     **/

    public Propiedad() {
        this.codigo = "";
        this.nombre = "";
        this.cuartos = "";
        this.provincia = "";
        this.alquiler = "";
    }

    /**
     * Este es el método contructor de la clase
     * Es el constructor que sí recibe parámetros
     * @param codigo es de tipo entero y representa el código de la propiedad
     * @param nombre es el tipo String y representa el nombre de la propiedad
     * @param cuartos es de tipo entero y representa la cantidad de cuartos
     * @param provincia es de tipo String y representa la provincia donde está ubicada la propiedad
     * @param alquiler es de tipo decimal y representa el monto mensual de alquiler
     **/


    public Propiedad(int codigo, String nombre, int cuartos, String provincia, double alquiler) {
        this.codigo = codigo;
        this.nombre = nombre;
        this.cuartos = cuartos;
        this.provincia = provincia;
        this.alquiler = alquiler;
    }


    //metodos de acceso y get y set


    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getCuartos() {
        return cuartos;
    }

    public void setCuartos(int cuartos) {
        this.cuartos = cuartos;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public double getAlquiler() {
        return alquiler;
    }

    public void setAlquiler(double alquiler) {
        this.alquiler = alquiler;
    }

    //toString()

    /**
     * Método para devolver en un String los atributos del objeto
     * @return devuelve un String con la información del objeto
     **/
    public String toString() {
        return "steller.ahias.bl.Propiedad{" +
                "codigo=" + codigo +
                ", nombre='" + nombre + '\'' +
                ", cuartos=" + cuartos +
                ", provincia='" + provincia + '\'' +
                ", alquiler=" + alquiler +
                '}';
    }
}
