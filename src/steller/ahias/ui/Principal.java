package steller.ahias.ui;

import steller.ahias.bl.Propiedad;
import java.io.*;

public class Principal {

    static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    static Propiedad[] listaPropiedades = new Propiedad[10];

    public static void main(String[] args) throws IOException {
        menu();
    }

    public static void menu() throws IOException {
        int opcion;
        do {
            System.out.println("*** Bienvenido al Sistema ***");
            System.out.println("1. Registra steller.ahias.bl.Propiedad.");
            System.out.println("2. Listar steller.ahias.bl.Propiedad.");
            System.out.println("3. Mostar monto alquiler.");
            System.out.println("4. Info de propiedad específica.");
            System.out.println("5. Salir.");
            System.out.println("Digite una opción: ");
            opcion = Integer.parseInt(in.readLine());
            procesarOpcion(opcion);
        }while (opcion!=5);


    }

    public static void procesarOpcion(int pOpcion) throws IOException {
        switch (pOpcion)
        {
            case 1: registarPropiedad();
            break;
            case 2: listarPropiedad();
            break;
            case 3: mostrarTotalAlquiler();
            break;
            case 4: mostrarInfoEspecifica();
            break;
            default:
                System.out.println("No existe esa opción");
        }

    }


    public static void registarPropiedad() throws IOException {
        System.out.println("Ingrese el código: ");
        int codigo = Integer.parseInt(in.readLine());
        System.out.println("Ingrese el nombre: ");
        String nombre = in.readLine();
        System.out.println("Ingreso la cantidad de cuartos");
        int cuartos = Integer.parseInt(in.readLine());
        System.out.println("Ingrese la provincia");
        String provincia = in.readLine();
        System.out.println("Ingrese el monto del alquiler");
        double alquiler = Double.parseDouble(in.readLine());

        Propiedad propiedad = new Propiedad(codigo, nombre, cuartos, provincia, alquiler);

        for(int i = 0; i < listaPropiedades.length; i++){
            if (listaPropiedades[i] == null) {
                listaPropiedades[i] = propiedad;
                break;
            }
        }

    }

    public static void listarPropiedad() {
        for (int i = 0; i< listaPropiedades.length; i++) {
            if(listaPropiedades[i] != null) {
                System.out.println(listaPropiedades[i].toString());
            }
        }
    }

    public static void mostrarTotalAlquiler() {


    }

    public static void mostrarInfoEspecifica() {
    }



}//Fin del class
